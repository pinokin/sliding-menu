var sidebar = document.getElementById('sidebar');
var content = document.getElementById('main');

// --- Open functionality ---

var navOpenBtn = document.getElementById('nav-open-btn');
var mcNavOpen = new Hammer(navOpenBtn);
mcNavOpen.on('tap press', function(ev) {
    content.classList.add('show-navigation');
    sidebar.classList.add('is-open');
});

// --- Close functionality ---

var navCloseBtn = document.getElementById('nav-close-btn');
var mcNavClose = new Hammer(navCloseBtn);
mcNavClose.on('tap press', function(ev) {
    content.classList.remove('show-navigation');
    sidebar.classList.remove('is-open');
});
